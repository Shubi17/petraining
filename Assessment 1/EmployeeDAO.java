package com.sapient.week2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class EmployeeDAO {
	
	public List<EmployeeBean> readData() throws Exception{
		
		BufferedReader br=new BufferedReader(new FileReader("C:\\Users\\shusingh5\\Desktop\\test.csv"));
		String line;
		String[] arr;
		List<EmployeeBean> record=new ArrayList<EmployeeBean>();
		EmployeeBean emp=null;
		while((line=br.readLine())!=null) {
			arr=line.split(",");
			emp.setID(Integer.parseInt(arr[0]));
			emp.setName(arr[1]);
			emp.setSalary(Integer.parseInt(arr[2]));
			record.add(emp);
		}
		br.close();
		return record;
	}
	
	public float getToSal(List<EmployeeBean> record){
		
		float sum=0;
		//Get total salary 
		for(EmployeeBean e : record) {
			sum+=e.getSalary();
		}
		return sum;
	}
	
	public int getCount(List<EmployeeBean> ob,int Salary) {
		int count=0;
		for(EmployeeBean e : ob) {
			if(e.getSalary()==Salary) {
				count++;
			}
		}
		return count;
	}
	
	public EmployeeBean getEmployee(String fname, int id) throws IOException {
	       BufferedReader bw = new BufferedReader(new FileReader(fname));
	       EmployeeBean emp = null;
	       String str = bw.readLine();
	       while(str!=null && emp==null){
	           String[] data = str.split(",");
	           if(Integer.parseInt(data[0])==id){
	               emp.setID(Integer.parseInt(data[0]));
	               emp.setName(data[1]);
	               emp.setSalary(Integer.parseInt(data[2]));
	           }
	           str = bw.readLine();
	       }
	       bw.close();
	       return emp;
	}
}
