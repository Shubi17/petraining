package com.sapient.student.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.bean.StudentBean;
import com.sapient.student.dao.StudentDAO;

/**
 * Servlet implementation class StudentEditController
 */
public class StudentEditController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentEditController() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    ServletContext cn;
    @Override
    public void init(ServletConfig config) throws ServletException {
    	// TODO Auto-generated method stub
    	cn= config.getServletContext();
    	super.init(config);
    	
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter out=response.getWriter();
		try {
			String id= request.getParameter("id1");
			String name= request.getParameter("id2");
			String rollno= request.getParameter("id3");
			String percent= request.getParameter("id4");
			//StudentDAO.updateStudent(id, name, rollno, percent);
			Map<String,StudentBean> map = (Map<String, StudentBean>)cn.getAttribute("smlist");
			StudentBean obBean = map.get(rollno);
			obBean.setId(id);
			obBean.setName(name);
			obBean.setPercent(percent);
			map.put(rollno, obBean);
			RequestDispatcher rs = request.getRequestDispatcher("DeleteEdit.jsp");
			rs.forward(request, response);
			
		}
		catch(Exception e) {
			e.getStackTrace();
		}
		
	}

}
