package com.sapient.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Map;
import java.util.TreeMap;

import com.sapient.student.bean.StudentBean;

public class StudentDAO {
	public Map<String, StudentBean> getAllStudents() throws Exception{
		Connection con = DBConnection.getConnection();
		PreparedStatement ps = con.prepareStatement("select * from students");
		Map<String,StudentBean> map = new TreeMap<String, StudentBean>();
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData meta = rs.getMetaData();
		map.put("column_name", new StudentBean(meta.getColumnName(1), meta.getColumnName(2),meta.getColumnName(3), meta.getColumnName(4)));
		while(rs.next()) {
			String s1 = rs.getString(1);
			String s2 = rs.getString(2);
			String s3 = rs.getString(3);
			String s4 = rs.getString(4);
			map.put(s3, new StudentBean(s1, s2, s3, s4));
			
		}
		return map;
	}
	
	public static int updateStudent(String id,String name,String rollno,String percent) throws Exception {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("update table students set id=?,name=?,rollno=?,percent=? where rollno=?");
			ps.setString(1,id);
			ps.setString(2,name);
			ps.setString(3,rollno);
			ps.setString(4,percent);
			ps.setString(5,rollno);
			
			int rs = ps.executeUpdate();
			return rs;
	}
}
