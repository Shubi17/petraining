package com.sapient.student.exceptions;

public class StudentIDNotFound extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public StudentIDNotFound() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StudentIDNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
