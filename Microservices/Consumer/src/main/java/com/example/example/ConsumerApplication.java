package com.example.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestClientException;

import com.example.demo.controller.ConsumerController;

@SpringBootApplication
public class ConsumerApplication{

	public static void main(String[] args) throws RestClientException, IOException {
		ApplicationContext ctx = SpringApplication.run(
				ConsumerApplication.class, args);
		
		/*Controller ccon=ctx.getBean(Controller.class);
		System.out.println(ccon);
		System.out.println(ccon.getEmployee());*/
		
	}
	
	/*@Bean
	public  Controller  consumerControllerClient()
	{
		return  new Controller();
	}*/
}