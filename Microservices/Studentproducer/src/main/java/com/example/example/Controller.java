package com.example.example;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Controller {
	@Autowired
	StudentDAO dao;
	
	@GetMapping("/display")
	public List<StudentBean> display(){
		return dao.list();
	}
}
