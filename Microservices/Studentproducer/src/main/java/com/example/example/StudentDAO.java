package com.example.example;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
@Service
public class StudentDAO {
	List<StudentBean> list;
	
public StudentDAO() {
		super();
		list = new ArrayList<StudentBean>();
		list.add(new StudentBean("1", "Sudarshan", "Agrawal"));
		list.add(new StudentBean("2", "Sachin", "Rana"));
		list.add(new StudentBean("3", "Tejas", "Parmar"));
}

public List<StudentBean> list(){
	return list;
}
}