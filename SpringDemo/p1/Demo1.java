package p1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo1 {

	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("bean.xml");
		/*Holiday ob;
		ob=(Holiday)context.getBean("holiday");
		System.out.println(ob.getDate());
		System.out.println(ob.getHoliday_name());
		//System.out.println(ob);
		*/
		
		/*ListOfHolidays ob;
		ob=(ListOfHolidays)context.getBean("list");
		System.out.println(ob);
		*/
		
		Employee ob;
		ob=(Employee)context.getBean("employee");
		System.out.println(ob);
		((ClassPathXmlApplicationContext)context).close();
	}

}
