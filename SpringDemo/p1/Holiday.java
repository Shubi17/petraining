package p1;

import java.util.Date;

public class Holiday {
	private String date;
	private String holiday_name;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Holiday(String date, String holiday_name) {
		super();
		this.date = date;
		this.holiday_name = holiday_name;
	}
	@Override
	public String toString() {
		return "Holiday [date=" + date + ", holiday_name=" + holiday_name + "]";
	}
	public String getHoliday_name() {
		return holiday_name;
	}
	public void setHoliday_name(String holiday_name) {
		this.holiday_name = holiday_name;
	}
	public Holiday() {
		super();
	}
}
