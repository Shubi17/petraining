package p1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaContainer {
	
	@Bean
	@Scope("singleton")
	public Hello get1() {
		return new Hello();
	}
	
	@Bean
	public Employee get2(){
		Employee ob;
		ob=new Employee(17,"Shubi");
		return ob;
	}
	
	@Bean
	public ListOfHolidays get3(){
		ListOfHolidays ob;
		ob=new ListOfHolidays();
		ob.getHolidays().add(new Holiday("29/06/2019","Bhaisaab Birthday"));
		ob.getHolidays().add(new Holiday("02/10/2019","Gandhi Jayanti"));
		return ob;
	}
}
