package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class ArithmeticAspect {
	
	@Before("execution(* *.*(double,double))") //Advice
	public void check1(JoinPoint jpoint) {
		for(Object x: jpoint.getArgs()) {
			double v=(Double) x;
			if(v<0) {
				throw new IllegalArgumentException("Number cannot be neagative");
			}
		}
	}
	
	@AfterReturning(pointcut = "execution(* *.*(double,double))",returning="retval") //Advice
	public void check2(JoinPoint jpoint,Object retval) {
		for(Object x: jpoint.getArgs()) {
			double v=(Double) x;
			if(v<0) {
				throw new IllegalArgumentException("Number cannot be neagative");
			}
		}
	}
}
