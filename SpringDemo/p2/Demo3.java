package p2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo3 {

	public static void main(String[] args) {
		try { 
			//Different try catch for all the methods to run even after exception
			Arithmetic ob;
			ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
			ob = (Arithmetic) context.getBean("arithmetic");
			
			System.out.println(ob.add(5, 6.0));
			System.out.println(ob.sub(-5, 6.0));
			System.out.println(ob.mul(5, 6));
			System.out.println(ob.div(5, 0));
		}
		catch (Exception e) {
				System.out.println(e.getMessage());
		}
	}
}
