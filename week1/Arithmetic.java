package com.sapient.week1;

public class Arithmetic {
	//1. Name of the method
	//2. Number of arguements
	//3. Type of arguements
	//4. Order of the arguements
	//5. Return type is not part of the signature
	
	public int add(int a, int b) {
		return a+b;
	}
	public float add(float a, int b) {
		return a+b;
	}
	public float add(int a, float b) {
		return a+b;
	}
	public float add(float a, float b) {
		return a+b;
	}
	public String add(String a, String b) {
		return a+b;
	}
}
