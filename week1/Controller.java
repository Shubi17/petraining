package com.sapient.week1;

public class Controller {
	
	private Model model;
	private View view;
	
	Controller(Model model,View view){
		this.model=model;
		this.view=view;
	}
	
	public void setFirstNumber(int First) {
		model.setFirstNumber(First);
	}
	public int getFirstNumber() {
		return model.getFirstNumber();
	}
	
	public void setSecondNumber(int Second) {
		model.setSecondNumber(Second);
	}
	public int getSecondNumber() {
		return model.getSecondNumber();
	}
	
	public void info() {
		view.display(model.getFirstNumber(), model.getSecondNumber());
	}
	
	public static void main(String[] args) {
		Model model=RetrieveInfo();
		View view=new View();
		Controller controller=new Controller(model,view);
		controller.info();
		System.out.println("Sum is : "+(controller.getFirstNumber()+controller.getSecondNumber()));
		
	}
	
	public static Model RetrieveInfo() {
		Model model=new Model();
		model.setFirstNumber(Read.sc.nextInt());
		model.setSecondNumber(Read.sc.nextInt());
		return model;
	}

}
