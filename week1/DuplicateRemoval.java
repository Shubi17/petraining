package com.sapient.week1;

import java.util.HashSet;
import java.util.Set;

public class DuplicateRemoval {

    Set<Integer> set;    
    DuplicateRemoval(){
        set= new HashSet<>();
    }
    public Set<Integer> removal(int arr[]){
        for(int i=0;i<arr.length;i++) {
            set.add(arr[i]);
        }
        return set;
    }

    public void print() {
        for (Integer integer : set) {
            System.out.print(integer+" ");
        }
    }    
    
    public static void main(String[] args) {
        DuplicateRemoval d= new DuplicateRemoval();
        int arr[]=new int[10];
        for(int i=0;i<10;i++) {
            arr[i]=Read.sc.nextInt();
        }
        d.removal(arr);
        d.print();
        System.out.println();
    }
}