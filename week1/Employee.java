package com.sapient.week1;

public class Employee {
	
	private int EmpID;
	private String EmpName;
	private int Empage;
	
	public int getEmpID() {
		return EmpID;
	}

	public void setEmpID(int empID) {
		EmpID = empID;
	}

	public String getEmpName() {
		return EmpName;
	}

	public void setEmpName(String empName) {
		EmpName = empName;
	}

	public int getEmpage() {
		return Empage;
	}

	public void setEmpage(int empage) {
		Empage = empage;
	}

}
