package com.sapient.week1;

class MyGen<T>{  
	T obj;  
	void add(T obj){
		this.obj=obj;
	}  
	T get(){
		return obj;
	}  
}

public class Generics{
	
	public static void main(String[] args) {
		
		MyGen<Integer> m=new MyGen<Integer>();  
		m.add(2);  
		System.out.println(m.get());  
		
		MyGen<Integer[]> m1=new MyGen<Integer[]>();
		Integer[] a= {1,2,3,4,5};
		m1.add(a);
		for(Integer number : a) {
			System.out.print(number+" ");
		}
		//System.out.println(m1.get());
	}
}
