package com.sapient.week1;

public class Model {
	private int FirstNumber;
	private int SecondNumber;
	
	public int getFirstNumber() {
		return FirstNumber;
	}
	public void setFirstNumber(int firstNumber) {
		FirstNumber = firstNumber;
	}
	public int getSecondNumber() {
		return SecondNumber;
	}
	public void setSecondNumber(int secondNumber) {
		SecondNumber = secondNumber;
	}
}
