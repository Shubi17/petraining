package com.sapient.week1;

public class PrimeOrNot {    
	public boolean primeornot(int x) {
	    if(x==0 || x==1) {
	        return false;
	    }
	    for(int i=2;i<x/2;i++) {
	        if(x%i==0) {                
	            return false;
	        }            
	    }
	    return true;
	}
	
	public static void main(String args[]) {
	    PrimeOrNot p= new PrimeOrNot();
	    int d;
	    d=Read.sc.nextInt();
	    System.out.println(p.primeornot(d));
	}
}