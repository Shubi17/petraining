package com.sapient.week1;

class Operation{
	void result(int a,int b) {}
};

class sum extends Operation{
	void result(int a,int b){
		System.out.println(a+b);
	}
};

class subtract extends Operation{
	void result(int a,int b){
		System.out.println(a-b);
	}
};

class multiply extends Operation{
	void result(int a,int b){
		System.out.println(a*b);
	}
};

class divide extends Operation{
	void result(int a,int b){	
		System.out.println(a/b);
	}
};

public class RuntimePoly{
	public static void main(String args[]){
		Operation obj1= new sum();
		obj1.result(20,10);
		Operation obj2= new subtract();
		obj2.result(30,10);
		Operation obj3= new multiply();
		obj3.result(5,6);
		Operation obj4= new divide();
		obj4.result(12,3);
		
	}
}