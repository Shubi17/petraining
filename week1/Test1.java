package com.sapient.week1;

import static org.junit.jupiter.api.Assertions.*; 
import org.junit.jupiter.api.Test;

class Test1 {    
	PrimeOrNot p= new PrimeOrNot();
    @Test
    public void test() {
        assertEquals(false,p.primeornot(1));
    }
    @Test
    public void test2() {
        assertEquals(false,p.primeornot(0));
    }
    @Test
    public void test3() {
        assertEquals(true,p.primeornot(29));
    }
    }