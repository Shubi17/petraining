package com.sapient.week1;

public class WrapperClasses {

	Integer intwrap(int a) {        
        int num=100;
        Integer array=Integer.valueOf(num);
        return array;        
    }
    Integer arraywrap() {           
        String array="100";
        int g=Integer.parseInt(array);
        return g;    
    }
    Character charwrap() {
    	Character initial='S';
    	return initial;
    }
    
    public static void main(String[] args) {
        WrapperClasses wrap= new WrapperClasses();
        System.out.println(wrap.intwrap(200));
        System.out.println(wrap.arraywrap()+10);
        System.out.println(wrap.charwrap());
    }
}
