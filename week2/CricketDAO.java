package com.sapient.week2;

import java.util.*;
import java.sql.*;

public class CricketDAO {
	
	public List<PlayerBean> getPlayers() throws Exception{
		Connection co = Db.getMyconnection();
		PreparedStatement ps = co.prepareStatement("select ID,FirstName,LastName,JerseyNo from player");
		ResultSet rs = ps.executeQuery();
		List<PlayerBean> lo = new ArrayList<>();
		PlayerBean player = null;
		while(rs.next()) {
			 player= new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
			 lo.add(player);
		}
		return lo;
	}
	public List<PlayerBean> getAnyPlayer(int id) throws Exception{
		Connection co = Db.getMyconnection();
		PreparedStatement ps = co.prepareStatement("select ID,FirstName,LastName,JerseyNo from player where ID =? ");
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		List<PlayerBean> lo = new ArrayList<>();
		PlayerBean player = null;
		while(rs.next()) {
			 player= new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
			 lo.add(player);
		}
		return lo;
	}
	public int insertPlayer(PlayerBean ob) throws Exception{
		Connection co = Db.getMyconnection();
		PreparedStatement ps = co.prepareStatement("insert into player (ID,FirstName,LastName,JerseyNo) values (?,?,?,?) ");
		ps.setInt(1, ob.getID());
		ps.setString(2, ob.getFirstName());
		ps.setString(3, ob.getLastName());
		ps.setInt(4, ob.getJerseyNo());
		int rs = ps.executeUpdate();
		return rs;
	}
	
	public int UpdatePlayer(int id, PlayerBean ob) throws Exception{
	       Connection co = Db.getMyconnection();
	       PreparedStatement ps = co.prepareStatement("update player set firstname=?,lastname=?,jerseyno=? where id=?");
	       ps.setInt(4, id);
	       ps.setString(1, ob.getFirstName());
	       ps.setString(2, ob.getLastName());
	       ps.setInt(3, ob.getJerseyNo());
	       int rs = ps.executeUpdate();
	       return rs;
	   }

	public int DeletePlayer(int id) throws Exception{
	       Connection co = Db.getMyconnection();
	       PreparedStatement ps = co.prepareStatement("delete from player where id=?");
	       ps.setInt(1, id);
	       int rs = ps.executeUpdate();
	       co.commit();
	       System.out.println("deleted");
	       return rs;
	   }
	
}
