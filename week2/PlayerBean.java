package com.sapient.week2;

public class PlayerBean {
	private int ID;
	private String FirstName;
	private String LastName;
	private int JerseyNo;
	
	@Override
	public String toString() {
		return "PlayerBean [ID = " + ID + ", FirstName= " + FirstName + ", LastName= " + LastName + ", jerseyNo= " + JerseyNo + "]";
	}
	
	public PlayerBean(int ID,String FirstName,String LastName,int JerseyNo) {
		this.ID=ID;
		this.FirstName=FirstName;
		this.LastName=LastName;
		this.JerseyNo=JerseyNo;
	}
	
	public PlayerBean() {
		super();
	}
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public int getJerseyNo() {
		return JerseyNo;
	}
	public void setJerseyNo(int jerseyNo) {
		JerseyNo = jerseyNo;
	}
	
}
